<?php

$output    = '';
$template_patch = dirname(__FILE__).'/template/index.html';
$conf_pach = dirname(__FILE__).'/config.json';
$config = json_decode(file_get_contents($conf_pach), true);

if (isset($_POST['send'])) {

    $config['limit'] = $_POST['limit'];
    $config['img_size_x'] = $_POST['img_size_x'];
    $config['img_size_y'] = $_POST['img_size_y'];

    $config['only_active_module'] = (!empty($_POST['only_active_module'])) ? 1 : 0;
    $config['use_local_preview'] = (!empty($_POST['use_local_preview'])) ? 1 : 0;
    $config['use_preview'] = (!empty($_POST['use_preview'])) ? 1 : 0;

    file_put_contents($conf_pach, json_encode($config));

    $f = fopen($template_patch, "w");
    fwrite($f,$_POST['template']);

    $output .= '
        <script>
            $(document).ready(function() {
                Materialize.toast("Сохранено!", 4000);
            });
        </script>';
}

$template = file_get_contents($template_patch);

$output .= '
    <style>
        .markers input {
            background: #D9D9D9;
            border-radius: 5px;
            display: inline-block;
            font-weight: 700;
            padding: 5px;
            border:none;
            width:210px;
            text-align:center;
            margin: 1px;
        }
    </style>';

$output .= '
    <script type="text/javascript" src="js/codemirror/codemirror.js"></script>
    <script type="text/javascript" src="js/codemirror/mode/javascript/javascript.js"></script>
    <script type="text/javascript" src="js/codemirror/mode/xml/xml.js"></script>
    <script type="text/javascript" src="js/codemirror/mode/htmlmixed/htmlmixed.js"></script>
    <script type="text/javascript" src="js/codemirror/mode/css/css.js"></script>

    <link rel="StyleSheet" type="text/css" href="js/codemirror/codemirror.css" />
    <script type="text/javascript">
        $(document).ready(function(){
            var editor = CodeMirror.fromTextArea(document.getElementById("tmpl"), {
                lineNumbers: true,
                matchBrackets: true,
                indentUnit: 4,
                mode: "text/html"
            });
            editor.setSize(\'100%\', \'100%\');
        });
    </script>';

$only_active_module = ((isset($config['only_active_module']) and $config['only_active_module']) ? 'checked="checked" ' : '');
$use_local_preview = ((isset($config['use_local_preview']) and $config['use_local_preview']) ? 'checked="checked" ' : '');
$use_preview = ((isset($config['use_preview']) and $config['use_preview']) ? 'checked="checked" ' : '');

$output .= '
    <form action="" method="post">

        <h4 class="light">Управление плагином вывода похожих материалов</h4>
        <h5 class="light">Вывести виджет на страницу можно с помощью метки {{ relmat }}</h5>
        <div class="row">

            <div class="input-field col s12">
                <input id="limit" type="text" size="100" name="limit" value="' . $config['limit'] . '">
                <label for="limit">Максимальное количество рекомендаций:</label>
            </div>

            <p class="col s12">
                <input type="checkbox" name="only_active_module" id="only_active_module" '.$only_active_module.'value="1">
                <label for="only_active_module">Рекомендации только из активного модуля:<br><small>Например, при просмотре новости в рекомендациях будут только новости</small></label>
            </p>

            <div class="col b30tm s12"><h5 class="light">Настройки эскизов</h5></div>

            <p class="col s12">
                <input type="checkbox" name="use_local_preview" id="use_local_preview" '.$use_local_preview.'value="1">
                <label for="use_local_preview">Использовать локальные настройки эскизов изображений</label>
            </p>

            <p class="col s12">
                <input type="checkbox" name="use_preview" id="use_preview" '.$use_preview.'value="1">
                <label for="use_preview">Использовать эскизы изображений</label>
            </p>

            <div class="input-field col s6">
                <input id="img_size_x" type="number" name="img_size_x" value="' . (isset($config['img_size_x']) ? $config['img_size_x'] : '') . '">
                <label for="img_size_x">Ширина эскиза</label>
            </div>

            <div class="input-field col s6">
                <input id="img_size_y" type="number" name="img_size_y" value="' . (isset($config['img_size_y']) ? $config['img_size_y'] : '') . '">
                <label for="img_size_y">Высота эскиза</label>
            </div>

            <div class="col b30tm s12"><h5 class="light">Шаблон</h5></div>

            <div class="input-field col s12">
                <textarea wrap="off" id="tmpl" class="materialize-textarea" name="template">' . (isset($template) ? htmlspecialchars($template) : '') . '</textarea>
            </div>

        </div>

        <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
            <button class="btn-floating btn-large deep-orange form_save tooltipped" data-position="left" data-tooltip="Сохранить изменения" type="submit" name="send">
                <i class="large mdi-content-save"></i>
            </button>
        </div>

    </form>';

$output .= '
    <ul class="markers">
        <li><input onclick="select(this)" value="{{ materials }}" readonly="" /> - массив с материалами. С помощью {% for material in materials %} можно выводить материалы.</li>
        <li>Для вывода доступны все поля, записанные в БД для материала. Вот некоторые из них:</li>
        <li><input onclick="select(this)" value="{{ material.id }}" readonly="" /> - ID выводимого материала.</li>
        <li><input onclick="select(this)" value="{{ material.title }}" readonly="" /> - Заголовок.</li>
        <li><input onclick="select(this)" value="{{ material.views }}" readonly="" /> - Количество просмотров.</li>
        <li><input onclick="select(this)" value="{{ material.comments }}" readonly="" /> - Количество комментариев.</li>
        <li>Также доступны дополнительные метки:</li>
        <li><input onclick="select(this)" value="{{ material.atom.url }}" readonly="" /> - URL адрес материала.</li>
        <li><input onclick="select(this)" value="{{ material.atom.module }}" readonly="" /> - Модуль материала.</li>
        <li><input onclick="select(this)" value="{{ material.atom.img }}" readonly="" /> - URL адрес первой прикреплённой картинки, если есть.</li>
    </ul>';

?>